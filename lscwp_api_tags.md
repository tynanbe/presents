---
layout: cover
background: https://source.unsplash.com/Y31Z6Mf7rys/1920x2560
---

<!-- background: https://source.unsplash.com/gi1f13S1-64/2400x1800 -->

# LiteSpeed Cache<br>WordPress API

## Tags


---
layout: two-cols
---

# Basic Plugin

We'll target the `greet` query string parameter.

::right::

```php
<?php
/**
 * Plugin Name:  LiteSpeed Cache API Tags
 * ...
 */

namespace LscwpApiTags;

defined( 'WPINC' ) || exit;

add_action( 'init', function () {
	if ( ! isset( $_GET['greet'] ) ) {
		return;
	}

	$visitor = $_GET['greet'];

	# ...
```


---
layout: two-cols
---

# Dynamic Tag Name

We create a cache tag from the user's name.

::right::

```php {8-10,14}
add_action( 'init', function () {
	if ( ! isset( $_GET['greet'] ) ) {
		return;
	}

	$visitor = $_GET['greet'];

	$tag =
		__NAMESPACE__ . '\greet.'
		. mb_strtolower( $visitor, 'UTF-8' );

	$visitor = stripslashes( $visitor );

	do_action( 'litespeed_tag_add', $tag );

	# ...
```


---
layout: two-cols
---

# Greeting Content

We replace WordPress's content loop with our greeting.

::right::

```php {3-13}
add_action( 'init', function () {
	# ...
	add_action( 'loop_start', 'ob_start', 0, 0 );
	add_action( 'loop_end', function () use ( $visitor ) {
		ob_end_clean();
		?>
		<article class="page entry">
			<div class="entry-content">
				<?php greet( $visitor ); ?>
			</div>
		</article>
		<?php
	}, 0, 999 );
} );
```


---
layout: two-cols
---

# Greeting Function

We display a message and include a form that accepts a reply.

::right::

```php
function greet( $visitor ) {
	?>
	<h2><?php
		printf( esc_html__( 'Howdy, %1$s!' ), $visitor );
	?></h2>

	<form method="get">
		<input name="greet" type="hidden"
			value="<?php echo $visitor; ?>">

		<label for="reply">
			<h3><?php esc_html_e( 'Care to Respond?' ); ?></h3>
		</label>
		<input id="reply" name="reply" type="text" value=""
			placeholder="<?php
				esc_html_e( 'Type your reply here ...' );
			?>">

		<button type="submit"><?php
			esc_html_e( 'Send' );
		?></button>
	</form>
	<?php
}
```


---
layout: two-cols
---

# Reply Content

We revise our main function to add a condition that displays alternative content if the user replied to our greeting.

::right::

```php {7-11}
add_action( 'init', function () {
	# ...
		?>
		<article class="page entry">
			<div class="entry-content">
				<?php
				if ( isset( $_GET['reply'] ) ) {
					reply( $visitor );
				} else {
					greet( $visitor );
				}
				?>
			</div>
		</article>
		<?php
	}, 0, 999 );
} );
```


---
layout: two-cols
---

# Reply Function

We display a thank you note and show that we remember the user's reply ...

::right::

```php
function reply( $visitor ) {
	?>
	<h2><?php
		printf( esc_html__( 'Thank you, %1$s.' ), $visitor );
	?></h2>
	<h3><?php
		esc_html_e( "I’ll always remember your kind words." );
	?></h3>
	<?php
	$reply = stripslashes( $_GET['reply'] );
	if ( ! empty( $reply ) ) {
		?>
		<blockquote><?php
			echo esc_html( $reply );
		?></blockquote>
		<?php
	}
	# ...
```


---
layout: two-cols
---

# Reply Function <small>(cont.)</small>

And add a new form that will trigger a cache purge targeting every item associated with the visitor's name.

::right::

```php {4-17}
function reply( $visitor ) {
	# ...
	}
	?>

	<form method="post" action="<?php
			echo drop_query_var(
				$_SERVER['REQUEST_URI'],
				'reply'
			);
		?>">
		<input name="reset" type="hidden">
		<button type="submit"><?php
			esc_html_e( 'Forget about me ...' );
		?></button>
	</form>
	<?php
}
```


---
layout: two-cols
---

# Putting It All Together

We update our main function once more.

```http
https://example.com/?greet=luna
	x-litespeed-cache: miss
https://example.com/?greet=Luna&reply=It%27s%20electric
	x-litespeed-cache: miss
https://example.com/?greet=LUNA&reply=I%20will%20survive
	x-litespeed-cache: miss

https://example.com/?greet=Luna&reply=It%27s%20electric
	x-litespeed-cache: hit
```

When anyone named Luna submits the reset form, all LiteSpeed cache items for that name get purged!

```http
https://example.com/?greet=LUNA&reply=I%20will%20survive
	x-litespeed-cache: miss
```

::right::

```php {10-12}
add_action( 'init', function () {
	# ...

	$tag =
		__NAMESPACE__ . '\greet.'
		. mb_strtolower( $visitor, 'UTF-8' );

	$visitor = stripslashes( $visitor );

	if ( isset( $_POST['reset'] ) ) {
		do_action( 'litespeed_purge', $tag );
	}

	do_action( 'litespeed_tag_add', $tag );

	# ...
```


---
layout: center
class: text-center
---

## <small><heroicons-sparkles /></small> Thank You <small><heroicons-sparkles /></small>

___

[Full Demo Plugin <small><mdi-cloud-download-outline /></small>](https://gitlab.litespeedtech.com/tynanbe/presents/-/raw/main/assets/lscwp_api_tags.php?inline=false)

[LiteSpeed Cache API Reference <small><mdi-book-arrow-right-outline /></small>](https://docs.litespeedtech.com/lscache/lscwp/api/)
