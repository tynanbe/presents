# Observing PHP

> “You cannot see the lettuce and the dressing without suspecting a salad.”
>
> —Arthur Conan Doyle

## `wp-config.php`

```php
<?php
# ...

define( 'WP_DEBUG', true );
define( 'WP_DEBUG_DISPLAY', false );
define( 'WP_DEBUG_LOG', '../logs/wp-error.log' );

# ...
```

## Error Reporting Snippet

- **[Code Snippets](https://wordpress.org/plugins/code-snippets/)**
- **[Download this snippet for import](https://gitlab.litespeedtech.com/tynanbe/presents/-/raw/main/assets/error-reporting.code-snippets.json?inline=false)**

```php
<?php

# Keep errors private
ini_set( 'display_errors', '0' );
# Log all errors except for some warnings
error_reporting( E_ALL & ~E_DEPRECATED & ~E_STRICT );

# Logs variables in scope and a backtrace
function tmp_debug( $vars = array(), $comment = '' ) {
	ob_start();
	echo '[Defined Vars] ';
	print_r( $vars );
	echo "[Backtrace]\n";
	debug_print_backtrace();
	$log = ob_get_clean();
	$re = '/(?:^#1\s+[^:]+:\s+(.+)$|.*)/m';
	$fn = trim( preg_replace( $re, '$1', $log ) );
	$comment = empty( $comment ) ? '' : "{$comment} of ";
	error_log( "[DEBUG] 🪲 {$comment}{$fn}\n{$log}" );
}

# USAGE
if ( function_exists( '\tmp_debug' ) ) \tmp_debug( get_defined_vars() );
# OR
if ( function_exists( '\tmp_debug' ) ) \tmp_debug( get_defined_vars(), 'Comment' );
```

## `tail -f ../logs/wp-error.log`

```
[21-Nov-2022 22:31:12 UTC] [DEBUG] 🪲 LiteSpeed\Task->init()
[Defined Vars] Array
(
    [guest_optm] =>
    [trigger] => Array
        (
            [name] => litespeed_task_crawler
            [hook] => LiteSpeed\Crawler::start
        )

    [id] => crawler
)
[Backtrace]
#0 .../html/wp-content/plugins/litespeed-cache/src/task.cls.php(71): tmp_debug(Array)
#1 .../html/wp-content/plugins/litespeed-cache/src/core.cls.php(286): LiteSpeed\Task->init()
#2 .../html/wp-includes/class-wp-hook.php(308): LiteSpeed\Core->after_user_init('')
#3 .../html/wp-includes/class-wp-hook.php(332): WP_Hook->apply_filters(NULL, Array)
#4 .../html/wp-includes/plugin.php(517): WP_Hook->do_action(Array)
#5 .../html/wp-settings.php(617): do_action('...')
#6 .../html/wp-config.php(100): require_once('...')
#7 .../html/wp-load.php(50(: require_once('...')
#8 .../html/wp-cron.php(46): require_once('...')

[21-Nov-2022 22:47:21 UTC] [DEBUG] 🪲 Doing Cron of LiteSpeed\Purge->_add(Array)
[Defined Vars] Array
(
    [tags] => Array
        (
            [0] => _LOCALRES
        )

    [purge2] =>
    [curr_built] => X-LiteSpeed-Purge: public,f48_
)
[Backtrace]
#0 .../html/wp-content/plugins/litespeed-cache/src/purge.cls.php(492): tmp_debug(Array, '...')
#1 .../html/wp-content/plugins/litespeed-cache/src/purge.cls.php(331): LiteSpeed\Purge->_add(Array)
#2 .../html/wp-content/plugins/litespeed-cache/src/purge.cls.php(188): LiteSpeed\Purge->_purge_all_localres(true)
#3 .../html/wp-content/plugins/litespeed-cache/src/purge.cls.php(170): LiteSpeed\Purge->_purge_all(false)
#4 .../html/wp-includes/class-wp-hook.php(308): LiteSpeed\Purge::purge_all()
#5 .../html/wp-includes/class-wp-hook.php(332): WP_Hook->apply_filters('', Array)
#6 .../html/wp-includes/plugin.php(565): WP_Hook->do_action(Array)
#7 .../html/wp-cron.php(188): do_action_ref_array('...', Array)
```

## Further Reading

### [‘How to debug PHP (without a debugger)’](https://alexwebdevelop.com/php-debug/)

- [About PHP debugging](https://alexwebdevelop.com/php-debug/#php-debug)
- [Error reporting](https://alexwebdevelop.com/php-debug/#php-error-reporting)
- [Variable debugging](https://alexwebdevelop.com/php-debug/#variable-debugging)
- [Function debugging](https://alexwebdevelop.com/php-debug/#function-debugging)
- [Logging debug data](https://alexwebdevelop.com/php-debug/#debug-logs)
- [Sending debug data via email](https://alexwebdevelop.com/php-debug/#debug-emails)
