# Tynan's LiteSpeed Presentations

## Prerequisites for Running Slideshows

```shell
$ git clone https://gitlab.litespeedtech.com/tynanbe/presents.git
$ cd presents
$ npm ci
$ npm run lscwp_api_control # or another slideshow presentation
```

## Presentations

### [2024-11-05] [AWK the Line](https://gitlab.litespeedtech.com/tynanbe/presents/-/blob/main/awk_the_line.md)

### [2024-08-27] [JavaScript: Outside the Browser](https://gitlab.litespeedtech.com/tynanbe/presents/-/blob/main/js_outside_the_browser.md)

### [2024-06-04] [LiteSpeed Cache WordPress: Cloud Summary](https://gitlab.litespeedtech.com/tynanbe/presents/-/blob/main/lscwp_cloud_summary.md)

### [2024-01-23] [OpenLiteSpeed CGI: With Gleam](https://gitlab.litespeedtech.com/tynanbe/presents/-/blob/main/ols_cgi_with_gleam.md)

### [2023-08-28] [Tailscale: Mesh VPN](https://gitlab.litespeedtech.com/tynanbe/presents/-/blob/main/tailscale_mesh_vpn.md)

### [2023-07-25] [QUIC.cloud Online Services: Overview](https://gitlab.litespeedtech.com/tynanbe/presents/-/blob/main/online_services_overview.md)

### [2023-05-31] [LiteSpeed Cache WordPress API: Cache Control](https://gitlab.litespeedtech.com/tynanbe/presents/-/blob/main/lscwp_api_control.md)

### [2023-01-31] [LiteSpeed Cache WordPress API: Tags](https://gitlab.litespeedtech.com/tynanbe/presents/-/blob/main/lscwp_api_tags.md)

```shell
$ npm run tailscale_mesh_vpn
```

### [2022-08-23] [Observing PHP](https://gitlab.litespeedtech.com/tynanbe/presents/-/blob/main/observing_php.md)

### [2022-11-21] [Tools for the Quick Fix](https://gitlab.litespeedtech.com/tynanbe/presents/-/blob/main/tools_for_the_quick_fix.md)
