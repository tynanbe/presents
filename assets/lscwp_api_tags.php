<?php
/**
 * Plugin Name:  LiteSpeed Cache API Tags
 * Plugin URI:   https://www.example.com/
 * Description:  A demo plugin that uses the LiteSpeed Cache API.
 * Version:      0.0.1
 * Author:       LiteSpeed Tech
 * Author URI:   https://www.example.com
 * License:      GPLv3
 * License URI:  http://www.gnu.org/licenses/gpl.html
 */

namespace LscwpApiTags;

defined( 'WPINC' ) || exit;

add_action( 'init', function () {
	if ( ! isset( $_GET['greet'] ) ) {
		return;
	}

	$visitor = $_GET['greet'];

	$tag =
		__NAMESPACE__ . '\greet.'
		. mb_strtolower( $visitor, 'UTF-8' );

	$visitor = stripslashes( $visitor );

	if ( isset( $_POST['reset'] ) ) {
		do_action( 'litespeed_purge', $tag );
	}

	do_action( 'litespeed_tag_add', $tag );

	add_action( 'loop_start', 'ob_start', 0, 0 );
	add_action( 'loop_end', function () use ( $visitor ) {
		ob_end_clean();
		?>
		<article class="page entry">
			<div class="entry-content">
				<?php
				if ( isset( $_GET['reply'] ) ) {
					reply( $visitor );
				} else {
					greet( $visitor );
				}
				?>
			</div>
		</article>
		<?php
	}, 0, 999 );
} );

function greet( $visitor ) {
	?>
	<h2><?php
		printf( esc_html__( 'Howdy, %1$s!' ), $visitor );
	?></h2>

	<form method="get">
		<input name="greet" type="hidden"
			value="<?php echo $visitor; ?>">

		<label for="reply">
			<h3><?php esc_html_e( 'Care to Respond?' ); ?></h3>
		</label>
		<input id="reply" name="reply" type="text" value=""
			placeholder="<?php
				esc_html_e( 'Type your reply here ...' );
			?>">

		<button type="submit"><?php
			esc_html_e( 'Send' );
		?></button>
	</form>
	<?php
}

function reply( $visitor ) {
	?>
	<h2><?php
		printf( esc_html__( 'Thank you, %1$s.' ), $visitor );
	?></h2>
	<h3><?php
		esc_html_e( "I’ll always remember your kind words." );
	?></h3>
	<?php
	$reply = stripslashes( $_GET['reply'] );
	if ( ! empty( $reply ) ) {
		?>
		<blockquote><?php
			echo esc_html( $reply );
		?></blockquote>
		<?php
	}
	?>

	<form method="post" action="<?php
			echo drop_query_var(
				$_SERVER['REQUEST_URI'],
				'reply'
			);
		?>">
		<input name="reset" type="hidden">
		<button type="submit"><?php
			esc_html_e( 'Forget about me ...' );
		?></button>
	</form>
	<?php
}

function drop_query_var( $uri, $var ) {
	list( $base, $query ) = explode( '?', $uri, 2 );
	parse_str( $query, $query );
	unset( $query[ $var ] );
	return count( $query )
		? "$base?" . http_build_query( $query )
		: $base;
}
