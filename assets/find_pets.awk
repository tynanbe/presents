#!/usr/bin/awk -f

BEGIN {
  FS = ","
  ORS = "\n\n"
}

NR > 1 {
  name = $1

  $0 = tolower($0)
  status = $2
  species = $3

  status_counts[status]++
  species_per_status[status,species]++
}

status == "new" || (status != "adopted" && species == "dog") {
  print "The " species " " name " is " status "."
}

END {
  print "Summary"

  for (status in status_counts) {
    some_pets = ""
    have = ""

    for (xs in species_per_status) {
      split(xs, x, SUBSEP)

      if (x[1] == status) {
        count = species_per_status[xs]
        have = some_pets || count > 1 ? "are" : "is"
        count = count " " x[2] (count > 1 ? "s" : "")
        some_pets = some_pets ? some_pets ", " count : count
      }
    }

    printf "* %s %s %s\n", some_pets, have, status
  }
}
