---
highlighter: shikiji
layout: cover
background: https://source.unsplash.com/rX2cj2FD4do/2400x3600
class: bright
---

# OpenLiteSpeed CGI

## With Gleam


---
layout: two-cols
class: text-center
---

# Common Gateway Interface

## ~30 years after the spec was introduced, CGI may be making a comeback

::right::

![CGI](/ols_cgi_with_gleam/cgi-logo.png)


---
layout: center
---

# The CGI Lifecycle

1. LiteSpeed receives an HTTP request
1. LiteSpeed sets several env vars and starts a new process
1. The CGI script can read the request body from `stdin`
1. The CGI script writes a response to `stdout` and exits
1. LiteSpeed sends the response to the client

[<small><mdi-book-arrow-right-outline /></small> https://www.rfc-editor.org/rfc/rfc3875](https://www.rfc-editor.org/rfc/rfc3875)


---
layout: two-cols
---

# Pros

- Potentially small attack surface compared to large web frameworks
- Eases memory management issues
- Hot code reloading is simple
- Multiple programming languages can run side by side
- Attractive for compute-heavy workloads where
 boot time means less
- Good for low traffic, etc. where resources needn't be contstantly in use

::right::

# Cons

- Potentially slow startup time
- Less common than other options today
- Library offerings may be barebones
- Not an optimal choice for handling frequent small requests


---
layout: two-cols
---

## CGI is simple to set up with the OpenLiteSpeed WebAdmin Console

_Note: You may need to perform similar actions with a VHost Template instead._

[<small><mdi-book-arrow-right-outline /></small> https://openlitespeed.org/kb/guide-to-openlitespeeds-cgi-setup/](https://openlitespeed.org/kb/guide-to-openlitespeeds-cgi-setup/)

::right::

![OLS WebAdmin Console CGI](/ols_cgi_with_gleam/cgi-context-new-1024x459.png)

![OLS WebAdmin Console CGI context](/ols_cgi_with_gleam/cgi-context-settings-new-1024x468.png)


---
layout: image-left
image: /ols_cgi_with_gleam/pexels-brady-knoll-3369569.jpg
class: grid place-content-center text-center
---

# Let's write a CGI script in Gleam

## _LiteSpeed makes it easy to use old and new tech together_


---
layout: image-right
image: /ols_cgi_with_gleam/gleam-bg.jpg
class: grid place-content-center
---

# What is Gleam?

> Gleam is a type safe and scalable language for the Erlang virtual machine and JavaScript runtimes.

_Gleam is rapidly approaching its v1.0 release. You can learn and explore the language interactively without installing a thing!_

[<small><mdi-book-arrow-right-outline /></small> https://tour.gleam.run/](https://tour.gleam.run/)


---
layout: two-cols
---

# Getting Started

Make sure you have Gleam and Deno installed.

```shell
gleam new cgi_gleam_demo && cd cgi_gleam_demo/
gleam add gleam_http cgi envoy esgleam filepath \
      glance glance_printer simplifile
"${EDITOR}" gleam.toml
```

<small>

_Note: If you use the asdf package manager to install Gleam, you can use it for Deno too._

- [<small><mdi-book-arrow-right-outline /></small> https://gleam.run/getting-started/installing/](https://gleam.run/getting-started/installing/#installing-gleam)
- [<small><mdi-book-arrow-right-outline /></small> https://docs.deno.com/runtime/manual](https://docs.deno.com/runtime/manual#install-deno)

</small>

::right::

<div>

### gleam.toml

```toml {3,7-11}
name = "cgi_gleam_demo"
version = "1.0.0"
target = "javascript"

# ...

[javascript]
runtime = "deno"

[javascript.deno]
allow_all = true
```

</div>

<small>

_Code available at:_

[<small><mdi-book-arrow-right-outline /></small> https://github.com/tynanbe/cgi_gleam_demo](https://github.com/tynanbe/cgi_gleam_demo)

</small>


---
layout: image-right
image: /ols_cgi_with_gleam/pexels-cottonbro-studio-6684662.jpg
class: grid place-content-center
---

# What is Deno?

> Deno is the most productive, secure, and performant JavaScript runtime for the modern programmer

_Deno can run much of the same code as Node.js, but it has some unique features too!_

[<small><mdi-book-arrow-right-outline /></small> https://examples.deno.land](https://examples.deno.land)


---
layout: two-cols
---

# All the imports and the main function head

Start by creating the module that will become our CGI script.

First we'll specify the library modules we want to use.

_Don't worry, they're mostly lean._

::right::

<div>

### src/hi.gleam

```gleam
import gleam/bit_array
import gleam/int
import gleam/list
import gleam/option.{None, Some}
import gleam/pair
import gleam/result
import gleam/string
import gleam/uri
import gleam/http/request
import gleam/http/response.{Response}
import cgi
import envoy
import glance
import glance_printer

/// Echoes a `Request`.
///
pub fn main() -> Nil {
  todo
}
```

</div>


---
layout: two-cols
---

# Env vars and query vars

We'll use some of those variables LiteSpeed provides for us.

Gleam functions typically have the main data as their first argument. The pipe operator (`|>`) takes the lefthand value and passes it as the first argument to the function on the right, similar to a `bash` pipe (`|`).

We'll discuss the `use request <- cgi.handle_request` line later. For now, it's enough to understand that it gets our request from LiteSpeed.

::right::

<div>

### src/hi.gleam

```gleam
pub fn main() -> Nil {
  use request <- cgi.handle_request

  // Use a variable from the environment
  let hi = case envoy.get("SERVER_SOFTWARE") {
    Ok(server) -> "Hi from Gleam on " <> server
    _or -> "Hi from Gleam"
  }

  // Use a variable from the query string
  let is_plain = case request.query {
    Some(query) ->
      query
      |> uri.parse_query
      |> result.map(with: list.any(_, satisfying: fn(x) {
        pair.map_first(of: x, with: string.lowercase)
        == // x is a tuple here
        #("plain", "")
      }))
      |> result.unwrap(or: False)
    None -> False
  }

  todo
}
```

</div>


---
layout: two-cols
---

# Pretty printing

We want to make our response readable for humans.

While a pipe passes data into an expression, the `use` keyword pulls data out from an expression, in a sense. Wherever `use` appears, everything after it is the body of a callback that is the final argument to the function on the right of the `<-`, until the end of the block. This is perhaps Gleam's trickiest feature to grasp, at first.

_Note: The libs used in this `gleam_format` function add ~250K to our final bundle size &mdash; the cost of beauty._

::right::

<div>

### src/hi.gleam

```gleam
/// Formats Gleam data for pretty printing.
///
fn gleam_format(data: String) -> String {
  let begin = "fn main() {"
  let end = "}"

  let is_data = fn(line) { line != begin && line != end }
  let deindent = string.drop_left(from: _, up_to: 2)

  let result = {
    let module = begin <> data <> end
    use module <- result.map(glance.module(module))
    module
    |> glance_printer.print
    |> string.split(on: "\n")
    |> list.filter(keeping: is_data)
    |> list.map(with: deindent)
    |> string.join(with: "\n")
  }

  case result {
    Ok(data) -> data
    _or -> data
  }
}
```

</div>


---
layout: two-cols
---

# Pretty printing <small>(cont.)</small>

Back in the `main` function, we convert the raw request body into a human-readable string and apply our `gleam_format` function.

::right::

<div>

### src/hi.gleam

```gleam
  // ..

  // Convert the request body to a string
  // Format the request record for pretty printing
  let request =
    request
    |> request.set_body(
      request.body
      |> bit_array.to_string
      |> result.unwrap(or: ""),
    )
    |> string.inspect
    |> gleam_format

  todo
}
```

</div>


---
layout: two-cols
---

# Bye ... Wait!

We're not actually done yet. Let's spice it up a bit more by adding a function to help prove that our response is truly dynamic. 

::right::

<div>

### src/hi.gleam

```gleam
/// Returns a random parting message.
///
fn bye() -> String {
  let adjectives = [
    "awesome", "blessed", "excellent", "fantastic",
    "fine", "great", "inspired", "lovely", "outstanding",
    "phenomenal", "pleasant", "stellar", "wonderful",
  ]
  let max = list.length(of: adjectives)

  let assert Ok(adjective) =
    max
    |> int.random
    |> list.at(in: adjectives)

  let some_kind_of = case adjective {
    "a" <> _ | "e" <> _ | "i" <> _ | "o" <> _
    | "u" <> _ -> "an " <> adjective
    _or -> "a " <> adjective
  }

  "Have " <> some_kind_of <> " day~\n"
}
```

</div>


---
layout: two-cols
---

# That's a wrap

Back in the `main` function for the last time, we return our `Response` record.

I'm sure you haven't forgotten, however, that LiteSpeed and the CGI spec require us to print the response to `stdout`. That's where `cgi.handle_request` comes in to finish the job.

::right::

<div>

### src/hi.gleam

```gleam
pub fn main() -> Nil {
  use request <- cgi.handle_request

  // ..

  // Prepare the response
  Response(
    // OK
    status: 200,
    headers: [#("content-type", "text/plain")],
    body: case is_plain {
      True -> request
      False ->
        hi <> "! You sent me this:\n\n"
        <> request <> "\n" <> bye()
    },
  )
}
```

</div>


---
layout: two-cols
---

# The Bundler

Bundling the compiled JavaScript from our code and its deps lets us deploy a single file.

A handful of postprocessing steps could be done with a shell script, but I've written another Gleam module instead.

We won't review it in full. The important parts are:

1. Ensuring the script can be executed by the LiteSpeed server user (`nobody`, for me).
1. Getting the shebang right <mdi-hand-pointing-right />

<small>

[<small><mdi-book-arrow-right-outline /></small> https://github.com/tynanbe/cgi_gleam_demo/
blob/main/src/hi/bundle.gleam](https://github.com/tynanbe/cgi_gleam_demo/blob/main/src/hi/bundle.gleam)

</small>

::right::

<div>

### src/hi/bundle.gleam

```gleam
shebang(
  // `env -S` allows passing multiple args in shebangs
  with: "/usr/bin/env -S sh -c",
  run: [
    // Deno panics without a home directory,
    // e.g. running as `nobody`
    "HOME=/tmp",
    // `exec` prevents zombies
    "exec deno run",
    // CGI scripts read env vars for request metadata
    "--allow-env",
    // CGI scripts read requests' bodies from stdin
    "--allow-read=/dev/stdin",
    // CGI scripts write responses to stdout
    "--allow-write=/dev/stdout",
    // Network access allows importing external modules
    "--allow-net",
    // Run this JavaScript module
    name,
  ],
)
<> content
<> "main();\n"
```

</div>


---
layout: two-cols
---

# Deploy and enjoy~

Don't be surprised if the CGI script is compiled, bundled, and ready to
deploy in less than a second!

What to expect:

- Small bundle sizes, depending on needs
- Simple `scp` or `rsync` deployment
- Instant results
- ~~A pony~~

_Note: Test with `curl` for extra convenience._

::right::

<div>

```shell
gleam run -m hi/bundle
scp dist/hi remote:cgi-bin/
curl --data '{ok:1}' 'https://example.com/cgi-bin/hi?q=a'
curl --data '{ok:1}' 'https://example.com/cgi-bin/hi?q=a&plain'

Hi from Gleam on LiteSpeed! You sent me this:

Request(
  method: Post,
  headers: [
    #("content-type", "application/x-www-form-urlencoded"),
    #("authorization", ""),
    #("user-agent", "curl/8.0.1"),
    #("content-length", "6"),
    #("accept", "*/*"),
    #("host", "example.com")
  ],
  body: "{ok:1}",
  scheme: Https,
  host: "example.com",
  port: Some(443),
  path: "/",
  query: Some("q=a"),
)

Have a stellar day~
```

</div>


---
layout: center
---

# Future Considerations

- [Setting Up GeoLocation on OpenLiteSpeed](https://openlitespeed.org/kb/geolocation-setup/)
- [Bubblewrap in OpenLiteSpeed](https://openlitespeed.org/kb/bubblewrap-in-openlitespeed/)


---
layout: center
class: text-center
---

## <small><heroicons-sparkles /></small> Thank You <small><heroicons-sparkles /></small>

___

[CGI Gleam Demo <small><mdi-cloud-download-outline /></small>](https://github.com/tynanbe/cgi_gleam_demo)

[OpenLiteSpeed CGI Setup <small><mdi-book-arrow-right-outline /></small>](https://openlitespeed.org/kb/guide-to-openlitespeeds-cgi-setup/)

[CGI Spec <small><mdi-book-arrow-right-outline /></small>](https://www.rfc-editor.org/rfc/rfc3875)

[Gleam Tour <small><mdi-book-arrow-right-outline /></small>](https://tour.gleam.run/)

[Installing Gleam <small><mdi-book-arrow-right-outline /></small>](https://gleam.run/getting-started/installing/#installing-gleam)

[Deno examples <small><mdi-book-arrow-right-outline /></small>](https://examples.deno.land)

[Installing Deno <small><mdi-book-arrow-right-outline /></small>](https://docs.deno.com/runtime/manual#install-deno)
