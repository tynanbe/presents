# Tools for the Quick Fix

> “We become what we behold. We shape our tools and then our tools shape us.”
>
> —[Herbert Marshall McLuhan CC](https://www.wikiwand.com/en/Marshall_McLuhan) (July 21, 1911 – December 31, 1980)

## Search

Learning through experience to discern between useful and poor advice is a valuable, time-saving skill.

- StackOverflow and similar
  - Quickly scan the answers and comments
  - The accepted answer isn't always the best answer
  - The information isn't always current
- GitHub issues
- Developer blog results
- *Ignore the many monetized results and other aggregation sites*

## Regex

Regular expressions.

Pattern matching can do many things, from powering up search/replace to parsing source files in a programming language implementation.

- Good to learn
- Saves time in many situations, available often
- An important part of `.htaccess` rules
- More or less every programming language has some way to use regular expressions
- However, some coders don't know regex, or aren't comfortable with it
- The syntax can get messy fast, so it's often best to keep it simple and try to use other solutions for production code
- Sometimes pattern matching is the best tool for the job
- And it's great for quick tasks and personal tools
- Practice and test at [RegExr.com](https://regexr.com/)
- Cheatsheets are good starting points

### Tools

- `grep`
- `ripgrep`
- `sed`
- `sd`
- etc.

### Regex Example

Match every kind of localhost address.

This is not a simple regex, and in these cases it's very difficult to get it 100% right, especially on the first try. But for starters, we can enumerate some test cases and try to get all those right.

```regex
/^(?:localhost|[0:]+1|(?:[0:]+ffff:)?(?:7f00:1|127(?:\.\d+){1,3}))$/gim
```

```
localhost
LOCALHOST
127.0.0.1
127.0.0.001
127.0.00.1
127.00.0.1
127.000.000.001
127.0000.0000.1
127.0.01
127.1
127.001
127.0.0.254
127.63.31.15
127.255.255.254
0:0:0:0:0:0:0:1
0000:0000:0000:0000:0000:0000:0000:0001
::1
0::1
0:0:0::1
0000::0001
0000:0:0000::0001
0000:0:0000::1
0::0:1
::ffff:127.0.0.1
::ffff:7f00:1

# DO NOT MATCH
servername
subdomain.domain.tld
192.168.0.1
10.1.1.123
0001::1
dead:beef::1
::dead:beef:1
```

### Regex in Action

1. Understand a problem: [CSS minify combine breaks clip path](https://wordpress.org/support/topic/css-minify-combine-breaks-clip-path/)
   - CSS values like `url(#...)` are being rewritten to relative URLs, this should never happen
1. Reproduce the problem minimally
1. Search for the source of the problem:
   ```shell
   $ rg --context=3 --ignore-case --iglob='!**/*.{min.js,md,pot,svg,txt}' -- 'minif'
   ```
   - Glob is a different type of pattern matching used by shells
   - Show 3 lines before and after each search result (`--context=3`)
   - Find results regardless of upper or lowercase letters
   - Do not match (`--iglob='!`) any file that ends with `.min.js`, `.md`, `.pot`, `.svg`, or `.txt` (`*.{min.js,md,pot,svg,txt}`) in the current directory or any subdirectory (`**/`)
1. Review results:
   ```php
   # src/optimizer.cls.php
   public static function minify_css( $data ) {
     # ...
     $obj = new Lib\CSS_MIN\Minifier();
   ```
1. Search for the `Minifier` class
   ```shell
   $ rg --context=3 --ignore-case --iglob='**/*.php' -- 'class\s+minifier'
   ```
1. Investigate:
   - Open file `lib/css-min/minifier.cls.php`
     ```shell
     $ vim lib/css-min/minifier.cls.php
     ```
   - Walk through the call stack
     - Search through functions `/function` and find the `run` function containing `return $this->minify($css);`
     - Search `/fun.+?minify` and find within `$css = $this->processDataUrls($css);`
       - Within our regex search, `.+?` is an ungreedy match for one or more characters between `fun` and `minify`, a quick way to find something like `function minify`
     - Search `/fun.+?processDataUrls` and review contents
     - Looks possible, but potentially complicated
1. Back up a bit:
   ```shell
   $ rg --context=3 --ignore-case --iglob='**/*.php' -- 'class\s+CSS_MIN'
   ```
1. No results so list the directory contents:
   ```shell
   $ ls src/css_min
   ```
1. Investigate `lib/css-min/urirewriter.cls.php`:
   - Search for "relative" in our suspected file:
     ```shell
     $ rg --context=3 --ignore-case -- 'relative' lib/css-min/urirewriter.cls.php
     ```
   - Scan results and find:
     ```php
     # lib/css-min/urirewriter.cls.php
     private static function _processUriCB($m) {
       # ...
       // if not root/scheme relative and not starts with scheme
       if (!preg_match('~^(/|[a-z]+\:)~', $uri)) {
         // URI is file-relative: rewrite depending on options
         if (self::$_prependPath === null) {
           $uri = self::rewriteRelative($uri, self::$_currentDir, self::$_docRoot, self::$_symlinks);
           # ...
     ```
   - Open file `lib/css-min/urirewriter.cls.php` to line 294:
     ```shell
     $ vim +294 lib/css-min/urirewriter.cls.php
     ```
   - Verify this is the right code
     - Search `/_processUriCB`, the function with the code we found, and see it used as a callback in `public static function rewrite`
     - The callback matches `url()` patterns:
       ```php
       # lib/css-min/urirewriter.cls.php
       public static function rewrite($css, $currentDir, $docRoot = null, $symlinks = array()) {
         # ...
         $pattern = '/url\\(\\s*([\'"](.*?)[\'"]|[^\\)\\s]+)\\s*\\)/';
         $css = preg_replace_callback($pattern, __CLASS__ . '::_processUriCB', $css);
       ```
1. [Apply a simple regex fix](https://github.com/litespeedtech/lscache_wp/pull/471/files)
   ```diff
   - // if not root/scheme relative and not starts with scheme
   - if (!preg_match('~^(/|[a-z]+\:)~', $uri)) {
   + // if not anchor id, not root/scheme relative, and not starts with scheme
   + if (!preg_match('~^(#|/|[a-z]+\:)~', $uri)) {
   ```
1. It's good to be aware of other factors:
   - This file belongs to an external library included in our plugin
   - Can check to see if that library has been updated and already fixed this problem
   - Without any automated testing, need to check if there is a regression in case our copy of this external library is updated for any other reason
1. Test the fix using our minimal reproduction
1. Submit a patch to fix the project

## CLI

- It's fast
- It's simple
- You can customize it to suit your needs
- You can automate repetitive tasks to save time and reduce errors
- A variety of tools are available on most systems
- Countless newer tools are available too

### Tools to Learn

- [`git`](https://lab.github.com/githubtraining/introduction-to-github)
- [`vi`](https://www.openvim.com/) ([`vim`](https://www.openvim.com/))
- Your system's package manager
  - Windows: [`scoop`](https://scoop.sh/), [Chocolatey `choco`](https://chocolatey.org/), etc.
  - MacOS: [MacPorts (`port`)](https://www.macports.org/), [Homebrew (`brew`)](https://brew.sh/), etc.
  - Linux: [`apt`](https://wiki.debian.org/AptCLI), [`yum`](https://www.redhat.com/sysadmin/how-manage-packages), [`pacman`](https://wiki.archlinux.org/title/Pacman), [`xbps`](https://docs.voidlinux.org/xbps/index.html), etc.

### Shells

- POSIX (`sh`)
- `bash`
- `fish`
- PowerShell (`pwsh`)
- etc.
