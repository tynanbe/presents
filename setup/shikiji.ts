import * as fs from "node:fs";
import { defineShikijiSetup } from "@slidev/types";

export default defineShikijiSetup(() => {
  return {
    themes: {
      dark: "catppuccin-mocha",
      light: "catppuccin-latte",
    },
    langs: [
      "apache",
      "awk",
      "css",
      "csv",
      "diff",
      "docker",
      "elixir",
      "erlang",
      "fish",
      {
        ...JSON.parse(
          fs.readFileSync("./public/langs/gleam.tmLanguage.json", "utf8"),
        ),
        name: "gleam",
      },
      "go",
      "html",
      "ini",
      "js",
      "json",
      "lisp",
      "lua",
      "md",
      "php",
      "powershell",
      "python",
      "rust",
      "scheme",
      "sh",
      "sql",
      "ssh-config",
      "toml",
      "ts",
      "wasm",
      "xml",
      "yaml",
    ],
  };
});
