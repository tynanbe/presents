---
highlighter: shikiji
layout: cover
background: https://source.unsplash.com/L4-16dmZ-1c/2400x1350
class: bright
---

# LiteSpeed Cache<br>WordPress

## Cloud Summary


---
layout: two-cols
---

# The Class Summary

## A concept from the `Root` class inherited by many other LSCWP classes

::right::

```sql
select option_value
from wp_options
where option_name = 'litespeed.cloud._summary';
```

```json
{
  "curr_request.ver": 1714580773,
  "last_request.ver": 1714011684,
  "token_ts": 1706109444,
  "is_linked": 0,
  "usage.img_optm": {
    "total_used": "0",
    "used": "0",
    "quota": 5000,
    "unlimited_quota": false,
    "pag_used": "0",
    "pag_bal": 0,
    "pkgs": "2",
    "daily_quota": 500,
    "remaining_daily_quota": 500
  },
  "usage.page_optm": {
    "total_used": "0",
    "used": "0",
    "quota": 500,
  // ...
}
```


---
layout: two-cols
---

# The Cloud Summary

## Holds details pertinent to QUIC.cloud (QC) CDN and other Online Services

::right::

- Status of LSCWP and QC linkage
- Timestamps from previous activity
- Service quota and usage info
- Unusable QC nodes
- IP addresses of QC nodes
- Status of QC request blockage
- Etc.


---
layout: two-cols
---

# Tweak the Summary ...

<div class="ml-1.5rem">

<small class="ml--1.5rem"><heroicons-forward /></small> To quickly test or fix specific issues

</div>

<div class="ml-1.5rem">

<small class="ml--1.5rem"><heroicons-arrows-right-left /></small> To work around certain safety measures

</div>

<div class="ml-1.5rem">

<small class="ml--1.5rem"><heroicons-eye-slash /></small> Quietly — users shouldn't be privy to the following tips and tricks

</div>

::right::

# Using ...

<div class="ml-1.5rem">

<small class="ml--1.5rem"><heroicons-puzzle-piece /></small> [**Code Snippets** plugin](https://wordpress.org/plugins/code-snippets/)

</div>

<div class="ml-1.5rem">

<small class="ml--1.5rem"><heroicons-scissors /></small> **"Only run once"** (`single-use`) PHP snippet

</div>

<div class="ml-1.5rem">

<small class="ml--1.5rem"><heroicons-exclamation-triangle /></small> Care, to _**avoid breaking the site**_ with incorrect PHP code

</div>

<div class="ml-1.5rem">

<small class="ml--1.5rem"><heroicons-shield-check /></small> Further care, to _**delete the snippet**_ after executing, doing a hard refresh, and observing the intended change

</div>


---
layout: two-cols
---

# ‘Disabled’ QC Nodes

## Are bypassed for 24 hours when selecting a QC node to request services from

<div class="mb--.5rem ml-1.5rem">

<small class="ml--1.5rem"><heroicons-bug-ant /></small> If all available nodes are disabled, `debug.log` shows

</div>

```txt
nodes are in 503 failed nodes
```

::right::

#### <heroicons-scissors /> Run-Once Snippet

```php
<?php
if (class_exists('\LiteSpeed\Cloud')) {
  \LiteSpeed\Cloud::save_summary(array(
    'disabled_node' => (object) array(),
  ));
}
```

<br>

#### <heroicons-circle-stack /> Database change

```json
// litespeed.cloud._summary
{
  // Before
  "disabled_node": {
    "https://node123.quic.cloud": 1713282726
  },
  // ...

  // After
  "disabled_node": {},
  // ...
}
```


---
layout: two-cols
---

# Choose a QC Node

```yaml
* 'server.img_optm'
  - 'https://node117.quic.cloud' # (EU)
  - 'https://node119.quic.cloud' # (US)

* 'server.ucss'
  'server.vpi'
  - 'https://node12.quic.cloud'  # (EU)
  - 'https://node19.quic.cloud'  # (US)
  - 'https://node123.quic.cloud' # (AP)

* 'server.ccss'
  'server.health'
  'server.lqip'
  - 'https://node3.quic.cloud'
  - 'https://node13.quic.cloud'
  - 'https://node394.quic.cloud'
  - 'https://node449.quic.cloud'
```

::right::

#### <heroicons-command-line /> Find a more idle node

```shell
lssupport@gs:~$ bin/monitor_imgoptm_centers
lssupport@gs:~$ bin/monitor_service_centers
lssupport@gs:~$ bin/monitor_legacy_services
```

<br>

#### <heroicons-scissors /> Run-Once Snippet

```php
<?php
if (class_exists('\LiteSpeed\Cloud')) {
  \LiteSpeed\Cloud::save_summary(array(
    'server.img_optm' => 'https://node117.quic.cloud',
  ));
}
```


---
layout: two-cols
---

# Update QC IPs

## QC node IP addresses are cached for 3 days

<div class="mb--.5rem ml-1.5rem">

<small class="ml--1.5rem"><heroicons-bug-ant /></small> If a node accessing the LSCWP API isn't recognized, `debug.log` shows

</div>

```txt
❌ Not our cloud IP
# ...
❌ 2nd time: Not our cloud IP
```

::right::

#### <heroicons-scissors /> Run-Once Snippet

```php
<?php
if (class_exists('\LiteSpeed\Cloud')) {
  \LiteSpeed\Cloud::save_summary(array(
    'ips_ts' => 0,
  ));
}
```

<br>

#### <heroicons-bolt /> LSCWP code triggered

```php
<?php
# ...
if (empty($this->_summary['ips'])
|| empty($this->_summary['ips_ts'])
|| $this->_summary['ips_ts'] < $check_point) {
  self::debug(
    'Force updating ip as ips_ts is older than 3 days'
  );
  $this->_update_ips();
}
```


---
layout: two-cols
---

# Unblock ‘Err Domains’

## QC may send `err_alias`

LSCWP then adds the site's home url to `err_domains` and ignore its QC requests for 10 days

<div class="mb--.5rem ml-1.5rem">

<small class="ml--1.5rem"><heroicons-bug-ant /></small> When attempting a QC request, the user's `debug.log` shows

</div>

```txt
home url is in err_domains, bypass request:
  https://example.com
```

::right::

#### <heroicons-bolt /> User's `my.quic.cloud` shows

> Please go to Settings tab to fix the error first: QUIC.cloud CDN is not enabled from the LSCWP plugin for example.com/

<br>

#### <heroicons-scissors /> Run-Once Snippet

```php
<?php
if (class_exists('\LiteSpeed\Cloud')) {
  \LiteSpeed\Cloud::save_summary(array(
    'err_domains' => array(),
  ));
}
```


---
layout: center
class: text-center
---

## <small><heroicons-sparkles /></small> Thank You <small><heroicons-sparkles /></small>

___

[Code Snippets plugin <small><heroicons-puzzle-piece /></small>](https://wordpress.org/plugins/code-snippets/)
