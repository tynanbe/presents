---
layout: cover
background: https://source.unsplash.com/Dksk8szLRN0/2400x1600
class: bright
---

# Tailscale

## Mesh VPN


---
layout: center
---

# Why Tailscale?

## &ldquo;Unlike traditional VPNs, which tunnel all network traffic through a central gateway server, Tailscale creates a peer-to-peer mesh network.&rdquo;

[<small><mdi-book-arrow-right-outline /></small> https://tailscale.com/kb/1151/what-is-tailscale/](https://tailscale.com/kb/1151/what-is-tailscale/)


---
layout: two-cols
class: text-center
---

# Traditional VPN

## The central gateway likely isn't close to all users, so latency is higher for some, and it can also be a bottleneck.

::right::

![traditional vpn map](/tailscale_mesh_vpn/traditional-vpn.png)


---
layout: two-cols
class: text-center
---

# Mesh VPN

## Devices are directly connected, so latency is minimal and single points of failure are reduced.

::right::

![tailscale vpn map](/tailscale_mesh_vpn/tailscale-vpn.png)


---
layout: default
class: grid gap-18 place-content-center place-items-center
---

# How Tailscale Works

[<small><mdi-book-arrow-right-outline /></small> https://tailscale.com/blog/how-tailscale-works/](https://tailscale.com/blog/how-tailscale-works/)


---
layout: two-cols
---

## The Data Plane

- [WireGuard](https://www.wireguard.com/) creates lightweight tunnels between endpoints.
- Connections are:
  - Made by exchanging public keys<br>(like SSH)
  - End-to-end encrypted
  - Maintained while roaming

::right::

![mesh network](/tailscale_mesh_vpn/mesh-network.svg)


---
layout: two-cols
---

## The Control Plane

- Public keys and metadata are shared through a central server.
- The software is [open source](https://github.com/tailscale/tailscale).
- Tailscale also provides [free and paid access](https://tailscale.com/pricing/) to their managed central server.
- Authentication with the central server is handled by 3rd parties, using:
  - OAuth2
  - OpenID Connect
  - SAML

::right::

![tailscale authorization](/tailscale_mesh_vpn/tailscale-auth.svg)


---
layout: two-cols
---

## The Bonus Features

- [NAT and firewall traversal](https://tailscale.com/blog/how-nat-traversal-works/) via:
  - [STUN](https://datatracker.ietf.org/doc/html/rfc5389) <small>(Session Traversal Utilities for NAT)</small>
  - [ICE](https://datatracker.ietf.org/doc/html/rfc8445) <small>(Interactive Connectivity Establishment)</small>
  - [DERP](https://github.com/tailscale/tailscale/tree/main/derp) <small>(Designated Encrypted Relay for Packets)</small>
- No firewall configuration or<br>open ports are needed.
- Centralized ACL policies for<br>granular control.
- &hellip; and more

::right::

![nat traversal](/tailscale_mesh_vpn/nat-traversal.svg)


---
layout: default
class: grid grid-cols-2 gap-18 place-content-center place-items-center
---

<div>

# Getting Started

1. [Sign up](https://login.tailscale.com/start) for a Tailscale account.
1. [Install Tailscale](https://tailscale.com/download/) on two or more devices.
1. All machines on your VPN can access one another <small>(by default)</small>.
1. Add more machines any time.

[<small><mdi-book-arrow-right-outline /></small> https://tailscale.com/kb/1017/install/](https://tailscale.com/kb/1017/install/)

</div>

<!-- -->

<img class="w-18rem" alt="Tailscale signup" src="/tailscale_mesh_vpn/tailscale-signup.jpg">


---
layout: default
class: grid grid-cols-2 gap-18 place-content-center place-items-center
---

<div>

# Next Steps

- [MagicDNS](https://tailscale.com/kb/1081/magicdns/) allows you to access your machines by hostname <small>(by default)</small>.
- You can also edit your [machine names](https://tailscale.com/kb/1098/machine-names/) if desired.
- Use [tags](https://tailscale.com/kb/1068/acl-tags/) to simplify ACL logic.

</div>

<!-- -->

<img class="w-18rem" alt="VPN machines" src="/tailscale_mesh_vpn/vpn-machines.jpg">


---
layout: two-cols
---

# Advanced Usage

## Work With a Traditional VPN

- Allow access to a VPS (router) from internal machines but not vice versa.
- Start or update the router:<br>`tailscale up --advertise-routes=1.2.3.4,5.6.7.8,...`
- Ensure your VPS's static IP can access the routes you specify.
- Opt in to the router from another machine:<br>`tailscale up --accept-routes`
- Adapt this [shell script](https://gitlab.litespeedtech.com/tynanbe/presents/-/raw/main/assets/tailscale_route?inline=false) to update your routes by domain name.

::right::

<div>

## Access Controls

```php
{
  "tagOwners": {
    "tag:router": ["autogroup:owner"],
    "tag:vlan":   ["autogroup:owner"],
  },

  "acls": [
    // Match absolutely everything.
    // {"action": "accept", "src": ["*"], "dst": ["*:*"]},
    {
      "action": "accept",
      "src": ["tag:vlan"],
      "dst": ["*:*"],
    },
  ],
}
```

</div>


---
layout: center
class: text-center
---

## <small><heroicons-sparkles /></small> Thank You <small><heroicons-sparkles /></small>

___

[Tailscale Quickstart Reference <small><mdi-book-arrow-right-outline /></small>](https://tailscale.com/kb/1017/install/)

[Route Updater Script <small><mdi-cloud-download-outline /></small>](https://gitlab.litespeedtech.com/tynanbe/presents/-/raw/main/assets/tailscale_route?inline=false)
