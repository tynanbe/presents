---
highlighter: shikiji
layout: cover
background: https://images.pexels.com/photos/346885/pexels-photo-346885.jpeg
class: bright
---

# JavaScript

## Outside the Browser


---
layout: default
class: grid grid-cols-3 gap-3 place-content-center place-items-center text-center
---

<h1 class="col-span-3">3 Major Node.js Compatible Runtimes</h1>

<div class="w-100px">

![Node.js](/js_outside_the_browser/nodejs.svg)

Node.js

</div>

<div class="w-100px">

![Deno](/js_outside_the_browser/deno.svg)

Deno

</div>

<div class="w-100px">

![Bun](/js_outside_the_browser/bun.svg)

Bun

</div>


---
layout: two-cols
---

<div class="flex items-center">

<div class="w-16">

![Node.js](/js_outside_the_browser/nodejs.svg)

</div>

<h1 class="pl-4 mt-4">Node.js</h1>

<small class="pl-4">

([nodejs.org](https://nodejs.org/))

</small>

</div>

<small class="mt--4">

<small><heroicons-forward /></small> Learn Node.js ([nodejs.org/en/learn](https://nodejs.org/en/learn/getting-started/introduction-to-nodejs))

</small>

### History

- Initial Release: May 27, 2009
- Latest Release: 22.7.0 on Aug. 22, 2024
- Written In: C++, JavaScript
- JavaScript Engine: V8
- Architecture: Event-driven
- I/O: Asynchronous
- Original Author: Ryan Dahl

::right::

### Features

- Built-in test runner
- Experimental TypeScript support
- Off-by-default security sandboxing
- npm package registry ([npmjs.com](https://www.npmjs.com/))
- Good Web-standard API support
- WebAssembly (WASM/WASI) support
- C/C++ library FFI support


---
layout: two-cols
---

<div class="flex items-center">

<div class="w-16">

![Deno](/js_outside_the_browser/deno.svg)

</div>

<h1 class="pl-4 mt-4">Deno</h1>

<small class="pl-4">

([deno.com](https://deno.com/))

</small>

</div>

<small class="mt--4">

<small><heroicons-forward /></small> Deno by Example ([docs.deno.com/examples](https://docs.deno.com/examples/))

</small>

### History

- Initial Release: May 13, 2018
- Latest Release: 1.46.0 on Aug. 22, 2024
- Written In: Rust, JavaScript, TypeScript
- JavaScript Engine: V8
- Architecture: Event-driven
- I/O: Asynchronous
- Original Author: Ryan Dahl


::right::

### Features

- First-class TypeScript support
- First-class security sandboxing
- Built-in formatter, linter, LS, test runner, etc.
- First-class HTTP imports
- JSR package registry ([jsr.io](https://jsr.io/))
- npm support (`npm:example@1.0`)
- Reusable standard library ([jsr.io/@std](https://jsr.io/@std))
- Key–Value store ([Deno KV](https://deno.com/kv))
- Fresh web framework ([fresh.deno.dev](https://fresh.deno.dev/))
- Extensive Web-standard API support
- WebAssembly (WASM/WASI) support
- Native library FFI support


---
layout: two-cols
---

<div class="flex items-center">

<div class="w-16">

![Bun](/js_outside_the_browser/bun.svg)

</div>

<h1 class="pl-4 mt-4">Bun</h1>

<small class="pl-4">

([bun.sh](https://bun.sh/))

</small>

</div>

<small class="mt--4">

<small><heroicons-forward /></small> Bun Guides ([bun.sh/guides](https://bun.sh/guides))

</small>

### History

- Initial Release: Sept. 14, 2021
- Latest Release: 1.1.26 on Aug. 24, 2024
- Written In: Zig, C++, TypeScript
- JavaScript Engine: JavaScriptCore
- Architecture: Event-driven
- I/O: Asynchronous
- Original Author: Jarred Sumner


::right::

### Features

- Drop-in Node.js replacement
- First-class TypeScript support
- Built-in package manager, test runner, bundler, etc.
- First-class npm support
- Cross-platform shell scripting ([Bun Shell](https://bun.sh/docs/runtime/shell))
- Good Web-standard API support
- Experimental WebAssembly (WASM/WASI) support
- Native library FFI support


---
layout: default
class: grid place-content-center
---

# Basic HTTP Server

<div class="flex items-center">

<div class="w-16 mr-4">

![Node.js](/js_outside_the_browser/nodejs.svg)

</div>

```javascript
import http from "node:http";
http.createServer((request, response) => {
  response.writeHead(200, {"Content-Type": "text/plain"});
  response.end("Hi from Node.js!");
}).listen(3000);
```

</div>

<div class="flex items-center">

<div class="w-16 mr-4">

![Deno](/js_outside_the_browser/deno.svg)

</div>

```javascript
Deno.serve({port: 3000}, () => new Response("Hi from Deno!"));
```

</div>

<div class="flex items-center">

<div class="w-16 mr-4">

![Bun](/js_outside_the_browser/bun.svg)

</div>

```javascript
Bun.serve({port: 3000, fetch: () => new Response("Hi from Bun!")});
```

</div>


---
layout: center
class: text-center
---

## <small><heroicons-sparkles /></small> Thank You <small><heroicons-sparkles /></small>

___

[<small><mdi-book-arrow-right-outline /></small> Bun <small>(bun.sh)</small>](https://bun.sh/)

[<small><mdi-book-arrow-right-outline /></small> Deno <small>(deno.com)</small>](https://deno.com/)

[<small><mdi-book-arrow-right-outline /></small> Node.js <small>(nodejs.org)</small>](https://nodejs.org/)

[<small><mdi-book-arrow-right-outline /></small> JSR <small>(jsr.io)</small>](https://jsr.io/)

[<small><mdi-book-arrow-right-outline /></small> npm <small>(npmjs.com)</small>](https://www.npmjs.com/)
