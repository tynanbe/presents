---
layout: cover
background: https://source.unsplash.com/Y31Z6Mf7rys/1920x2560
---

# LiteSpeed Cache<br>WordPress API

## Cache Control


---
layout: two-cols
class: text-center
---

![that is the question](/lscwp_api_control/intro.jpg)

::right::

# To cache,<br>or *not* to cache


---
layout: default
class: grid gap-18 place-content-center place-items-center
---

## LSCache considers a page to be *non-cacheable* if:

- It is an *admin page*
- It is a `POST` *request*
- `is_trackback()` is *true*
- `is_search()` is *true*
- *No theme* is used


---
layout: default
class: grid grid-cols-2 gap-18 place-content-center
---

<h2 class="col-span-2">

LSCache *also* considers a page to be *non-cacheable* if:

</h2>

- The URI is found in the *Do Not Cache URIs* list
- The URL has a query string found in the *Do Not Cache Query Strings* list
- The post has a category found in the *Do Not Cache Categories* list
- The post has a tag found in the *Do Not Cache Tags* list

<!-- -->

- The request has a cookie found in the *Do Not Cache Cookies* list
- The request has a user agent found in the *Do Not Cache User Agents* list
- The request is being made by a user whose role is checked in the *Do Not Cache Roles* list


---
layout: two-cols
class: text-center
---

![Angels and ministers of grace defend us](/lscwp_api_control/hooks.jpg)

::right::

# Cache Control<br>API Hooks


---
layout: two-cols
---

# Mark the current page as *non-cacheable*

In addition to the various *Do Not Cache* lists, this hook can be used in plugins and snippets to customize which pages shouldn't be cached.

::right::

```php
do_action(
	'litespeed_control_set_nocache',
	'And flights of angels sing thee to thy rest!'
);
```


---
layout: two-cols
---

# Mark the current page as *cacheable*

The inverse action might be needed by plugins that generate pages without invoking the `wp` action hook, which is normally run after the WordPress environment is set up.

::right::

```php
do_action(
	'litespeed_control_set_cacheable',
	'The lady doth protest too much, methinks.'
);
```


---
layout: two-cols
---

# *Force* the current page to be *cacheable*

This hook overrides most types of non-cacheable conditions.

::right::

```php
do_action(
	'litespeed_control_force_cacheable',
	'if it be not to come, it will be now'
);
```


---
layout: two-cols
---

# Reflection

With all the settings and API hooks available, it can be difficult to know whether the current page is set to
be cached or not. Use this filter to find out.

::right::

```php
apply_filters( 'litespeed_control_cacheable', false );
```


---
layout: two-cols
---

# BONUS

These hooks can mark the current page as part of a logged-in user's *private* cache, or try to force the page into the *public* cache.

::right::

```php
do_action(
	'litespeed_control_set_private',
	'Listen to many, speak to a few.'
);

do_action(
	'litespeed_control_force_public',
	'Brevity is the soul of wit.'
);
```


---
layout: center
class: text-center
---

## <small><heroicons-sparkles /></small> Thank You <small><heroicons-sparkles /></small>

___

[LiteSpeed Cache API Reference <small><mdi-book-arrow-right-outline /></small>](https://docs.litespeedtech.com/lscache/lscwp/api/#cache-control)
