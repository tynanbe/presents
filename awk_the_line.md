---
highlighter: shikiji
layout: cover
background: https://images.pexels.com/photos/2425132/pexels-photo-2425132.jpeg
---

# AWK the Line


---
layout: default
class: grid place-content-center place-items-center
---

## Work with data and logs

<br>

### Various standard command line tools
### can filter or manipulate text

<br>

##### `pets.csv`

```csv
name,status,species
Isaac Mewton,NEW,cat
Harry Pawter,ADOPTED,dog
Frodog,WAITING,dog
Meowy Poppins,AVAILABLE,cat
Chris P. Bacon,NEW,pig
Bark Twain,ADOPTED,dog
```


---
layout: default
class: grid grid-cols-2 gap-10 place-content-center place-items-center px-30
---

<h1 class="col-span-2">Find new pets and all dogs</h1>

<div>

### `grep` vs. `sed` vs. `awk`

<br>

```sh
f="assets/pets.csv"

# Or pipe from `head -n1 $f` (multiple traversals)
grep -Ei '^name,|,new,[^,]*$|,dog$' $f

sed -En '1p; /,new,[^,]*$|,dog$/I p' $f

awk 'NR == 1 || tolower($0) ~ /,new,[^,]*$|,dog$/' $f

# Prefer `gawk --csv` (`gawk -k`) if available        
awk -F, 'NR == 1 \
  || tolower($2) == "new" || tolower($3) == "dog"' $f
```

</div>

<div class="mt--4em">

#### Column headings
#### and matching rows
#### from `pets.csv`

<br>

```csv
name,status,species
Isaac Mewton,NEW,cat
Harry Pawter,ADOPTED,dog
Frodog,WAITING,dog
Chris P. Bacon,NEW,pig
Bark Twain,ADOPTED,dog
```

</div>


---
layout: default
class: grid grid-cols-2 gap-10 place-content-center place-items-center px-30
---

<h1 class="col-span-2">Exclude adopted dogs</h1>

<div>

## Regex gets increasingly complex

<br>

```sh
grep -Ei ',new,[^,]*$|,dog$' $f | grep -iv ',adopted,[^,]*$'

sed -En '/,new,[^,]*$|,dog$/I { /,adopted,[^,]*$/I! p }' $f

awk '{ line = tolower($0) }
  line ~ /,new,[^,]*$|,dog$/ && line !~ /,adopted,[^,]*$/' $f

# Prefer `gawk --csv` (`gawk -k`) if available                     
awk -F, '{ status = tolower($2); species = tolower($3) }
  status == "new" || (status != "adopted" && species == "dog")' $f
```

<br>

<p class="indent">
Simple field matching with AWK is more verbose, but easier to understand.
</p>

</div>

<div class="mt--4em">

#### Matching rows
#### from `pets.csv`

<br>

```csv
Isaac Mewton,NEW,cat
Frodog,WAITING,dog
Chris P. Bacon,NEW,pig 
```

</div>


---
layout: default
class: grid grid-cols-2 gap-10 place-content-center place-items-center px-30
---

<h1 class="col-span-2">Transform results</h1>

<div>

##### `find_pets.awk`

```awk
#!/usr/bin/awk -f                                              

BEGIN {
  FS = ","
  ORS = "\n\n"
}

NR > 1 {
  name = $1

  $0 = tolower($0)
  status = $2
  species = $3
}

status == "new" || (status != "adopted" && species == "dog") {
  print "The " species " " name " is " status "."
}
```

</div>

<div class="mt--4em">

#### Matching rows
#### from `pets.csv`,
#### modified for easy reading

<br>

```txt
The cat Isaac Mewton is new.

The dog Frodog is waiting.

The pig Chris P. Bacon is new.
⠀                              
```

</div>

---
layout: default
class: grid grid-cols-2 gap-10 place-content-center place-items-center px-30
---

<div class="mt--4em">

# Further
# insights

##### Summary output from
##### `pets.csv`

<br>

```txt
# ...                  

Summary

* 2 dogs are adopted
* 1 dog is waiting
* 1 pig, 1 cat are new
* 1 cat is available
```

</div>

<div>

##### `find_pets.awk` revised

```awk
# ...                                                        
NR > 1 {
  # ...
  status_counts[status]++
  species_per_status[status,species]++
}
# ...
END {
  print "Summary"
  for (status in status_counts) {
    some_pets = ""
    for (xs in species_per_status) {
      split(xs, x, SUBSEP)
      if (x[1] == status) {
        count = species_per_status[xs]
        have = some_pets || count > 1 ? "are" : "is"
        count = count " " x[2] (count > 1 ? "s" : "")
        some_pets = some_pets ? some_pets ", " count : count
      }
    }
    printf "* %s %s %s\n", some_pets, have, status
  }
}
```

</div>


---
layout: center
class: text-center
---

## <small><heroicons-sparkles /></small> Thank You <small><heroicons-sparkles /></small>

___

## Resources

[<small><mdi-book-arrow-right-outline /></small> Awk in 20 Minutes <small>(ferd.ca/awk-in-20-minutes.html)</small>](https://ferd.ca/awk-in-20-minutes.html)

[<small><mdi-book-arrow-right-outline /></small> Intro to AWK <small>(grymoire.com/Unix/Awk.html)</small>](https://www.grymoire.com/Unix/Awk.html)

[<small><mdi-book-arrow-right-outline /></small> Awk Cheat Sheet <small>(quickref.me/awk)</small>](https://quickref.me/awk)

[<small><mdi-cloud-download-outline /></small> `find_pets.awk`](https://gitlab.litespeedtech.com/tynanbe/presents/-/raw/main/assets/find_pets.awk?inline=false)
