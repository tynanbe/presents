---
layout: cover
background: https://source.unsplash.com/Y31Z6Mf7rys/1920x2560
---

# QUIC.cloud<br>Online Services

## Overview


---
layout: center
---

## Current Online Services Architecture

```mermaid {scale: 0.39}
%% Graph
flowchart LR
subgraph qc["QUIC.cloud"]
  subgraph legacy["Legacy Services"]
    subgraph node["Node"]
      lshttpd("LiteSpeed")
      subgraph ccss["CCSS Instances"]
        ccss_n("ccss/app.js")
        ccss_etc("etc...")
      end
      subgraph json["JSON Instances"]
        json_n("json/app.js")
        json_etc("etc...")
      end
      subgraph health["Health Instances"]
        health_n("health/app.js")
        health_etc("etc...")
      end
      subgraph lqip["LQIP Instances"]
        lqip_n("lqip/app.js")
        lqip_etc("etc...")
      end
      redis[("Redis")]
    end
    node_etc("etc...")
  end
  subgraph queue["Queue-Based Services"]
    queue_center("Center Node")
    subgraph queue_workers["Worker Nodes"]
      queue_worker("Worker")
      queue_worker_etc("etc...")
    end
  end
  subgraph image["Image Optimization Services"]
    image_center("Center Node")
    subgraph image_workers["Worker Nodes"]
      image_worker("Worker")
      image_worker_etc("etc...")
    end
  end
  iapi((("IAPI")))
end
subgraph clients["Clients"]
  direction LR
  client("Client")
  client_etc("etc...")
end

%% Flow
clients <===> node_etc & image_center
clients ===> lshttpd & queue_center
lshttpd ==> ccss & json & health & lqip
redis <----> ccss & json & health & lqip
ccss & json & health & lqip & queue_workers ===> clients
queue_center -..-> queue_workers
queue_workers ---> queue_center
image_center -..-> image_workers
image_workers ==> image_center
image_workers <-...-> clients
iapi -.-> ccss & json & health & lqip & node_etc & queue_center & image_center
clients ~~~ redis & image & image_center & image_workers

%% Style
classDef instance stroke-dasharray: 5 5
classDef abstract_instance fill: transparent, stroke-dasharray: 5 5
class ccss,clients,health,image,image_workers,json,legacy,lqip,qc,queue,queue_workers abstract_instance
class client_etc,ccss_etc,health_etc,image_worker_etc,json_etc,lqip_etc,node_etc,queue_worker_etc instance
```


---
layout: center
---

## Current Queue-Based Architecture

```mermaid {scale: 0.52}
%% Graph
flowchart LR
subgraph qc["QUIC.cloud"]
  subgraph cluster["Cluster"]
    subgraph workers["Worker Nodes"]
      subgraph worker["Worker"]
        subgraph ucss["UCSS Instances"]
          ucss_app("ucss/app.js")
          ucss_app_etc("etc...")
        end
        subgraph vpi["VPI Instances"]
          vpi_app("vpi/app.js")
          vpi_app_etc("etc...")
        end
        worker_redis[("Redis")]
        statsd("StatsD")
      end
      worker_etc("etc...")
    end
    subgraph center["Center Node"]
      lshttpd("LiteSpeed")
      subgraph queue["Queue Instances"]
        queue_app("queue/app.js")
        queue_app_etc("etc...")
      end
      rabbitmq[("RabbitMQ<br>FIFO")]
      center_redis[("Redis")]
    end
  end
  subgraph clusters["etc..."]
    center_n("Center Node")
    subgraph workers_n["Worker Nodes"]
      worker_n("Worker")
      worker_n_etc("etc...")
    end
  end
  iapi((("IAPI")))
end
subgraph clients["Clients"]
  direction LR
  client_1("Client")
  client_etc("etc...")
end

%% Flow
clients ====> lshttpd & center_n
lshttpd ==> queue
lshttpd --> queue
queue ===> rabbitmq
queue <--> center_redis
rabbitmq -.-> ucss & vpi & worker_etc
ucss & vpi & worker_etc ====> clients
ucss & vpi & worker_etc ---> lshttpd
ucss <--> worker_redis
ucss --> statsd
center_n -..-> workers_n
workers_n ====> clients
workers_n ---> center_n
iapi -..-> queue & center_n

%% Style
classDef instance stroke-dasharray: 5 5
classDef abstract_instance fill: transparent, stroke-dasharray: 5 5
class clients,cluster,clusters,qc,queue,ucss,vpi,workers,workers_n abstract_instance
class client_etc,queue_app_etc,ucss_app_etc,vpi_app_etc,worker_etc,worker_n_etc instance
```


---
layout: center
---

## Current Image Optimization Architecture

```mermaid {scale: 0.39}
%% Graph
flowchart BT
subgraph qc["QUIC.cloud"]
  subgraph cluster["Cluster"]
    subgraph workers["Worker Nodes"]
      subgraph worker["Worker"]
        worker_lshttpd("LiteSpeed")
        subgraph fast["Fast Consumer Instances"]
          fast_app(".../WorkerConsume.php")
          fast_app_etc("etc...")
        end
        subgraph standard["Standard Consumer Instances"]
          standard_app(".../WorkerConsume.php")
          standard_app_etc("etc...")
        end
        subgraph purger["Purger Instances"]
          purger_app(".../WorkerPurger.php")
          purger_app_etc("etc...")
        end
      end
      worker_etc("etc...")
    end
    subgraph center["Center Node"]
      center_lshttpd("LiteSpeed")
      center_app[".../lscwpController.php"]
      subgraph callback["Callback Instances"]
        callback_app(".../lscwpConsume.php")
        callback_error_app(".../lscwpConsumeErrors.php")
        callback_etc("etc...")
      end
      rabbitmq[("RabbitMQ<br>FIFO")]
      redis[("Redis")]
    end
  end
  subgraph clusters["etc..."]
    center_n("Center Node")
    subgraph workers_n["Worker Nodes"]
      direction LR
      worker_n("Worker")
      worker_n_etc("etc...")
    end
  end
  iapi((("IAPI")))
end
subgraph clients["Clients"]
  client_1("Client")
  client_etc("etc...")
end

%% Flow
clients ==> center_lshttpd & center_n
center_lshttpd ==> center_app
worker_lshttpd & worker_etc & workers_n -.-> clients
center_app & fast & standard & worker_etc ==> rabbitmq
center_app & callback <--> redis
rabbitmq -.-> callback & fast & standard & purger & worker_etc
callback ===> clients
center_n -.-> workers_n
workers_n --> center_n
iapi -.-> center_app & center_n
redis ~~~ clients & center_lshttpd & worker_lshttpd & center_app
callback ~~~ center_app

%% Style
classDef instance stroke-dasharray: 5 5
classDef abstract_instance fill: transparent, stroke-dasharray: 5 5
class callback,clients,cluster,clusters,fast,purger,standard,qc,ucss,vpi,workers,workers_n abstract_instance
class callback_etc,client_etc,fast_app_etc,purger_app_etc,standard_app_etc,worker_etc,worker_n_etc instance
```


---
layout: center
class: text-center
---

## <small><heroicons-sparkles /></small> Thank You <small><heroicons-sparkles /></small>

___

[QUIC.cloud Online Services Reference <small><mdi-book-arrow-right-outline /></small>](https://www.quic.cloud/docs/online-services/)
